import sys

import xbmcplugin
import xbmcaddon
import xbmcgui
import xbmc

from resources.lib.gacams import GACams

addon_handle = int(sys.argv[1])

xbmcplugin.setContent(addon_handle, 'video')

gacams = GACams()
videos = gacams.get_videos()
for video in videos:
    cam_item = xbmcgui.ListItem(video[0])
    cam_item.setProperty('IsPlayable', 'true')
    cam_item.setArt({'thumb': video[1], 'fanart': video[1]})
    cam_item.setInfo('video', {})
    xbmcplugin.addDirectoryItem(addon_handle, video[-1], cam_item)
xbmcplugin.endOfDirectory(addon_handle)
