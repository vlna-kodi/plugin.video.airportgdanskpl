import requests
from bs4 import BeautifulSoup


class GACams:
    URL_ROOT = 'https://www.airport.gdansk.pl'
    URL_CAMS = '%s/lotnisko/kamery-internetowe-p30.html' % URL_ROOT

    def get_videos(self):
        videos = []
        r = requests.get(self.URL_CAMS)
        soup = BeautifulSoup(r.text, 'html.parser')
        for video in soup.find_all('div', attrs={'class': 'videoMain__left'}):
            videos.append([video.find('div', attrs={'class': 'videoMain__desc'}).text.strip(),
                           video.find('div', attrs={'class': 'cameraPlayer'})['data-poster'],
                           video.find('div', attrs={'class': 'cameraPlayer'})['data-url']])
        return videos
