import unittest
from resources.lib.gacams import GACams

class IntegrationTestCase(unittest.TestCase):
    def test_get_videos(self):
        gacams = GACams()
        videos = gacams.get_videos()
        self.assertGreaterEqual(len(videos), 5)

        for video in videos:
            self.assertEqual('.m3u8', video[-1][-5:])


if __name__ == '__main__':
    unittest.main()
